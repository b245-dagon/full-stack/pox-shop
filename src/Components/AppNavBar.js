
import {Link , NavLink} from 'react-router-dom';
import { Navigate, useNavigate} from 'react-router-dom';
import UserContext from '../UserContext';
import {useContext ,Fragment} from 'react'


export default function AppNavBar({setIsShowCart ,cart}){





  const {user} =useContext(UserContext);
 
    return(
       
       
       <>
          

      <header>
      
        <nav className="shadow">

        <div className="flex justify-between items-center py-3 px-10 container mx-auto">

          <div>
            {/* <h1 class="flex-col text-2xl font-bold bg-gradient-to-tr from-indigo-600 to-green-600 bg-clip-text text-transparent hover:cursor-pointer">Pets</h1> */}
            <img className="h-20 w-20" src="https://i.ibb.co/2ShGyt8/Brown-Black-Simple-Modern-Pet-Shop-Logo.png"></img>
          </div>

          <div>
            
            <div className="hover:cursor-pointer sm:hidden">
              <span className="h-1 rounded-full block w-8 mb-1 bg-gradient-to-tr from-green-600 to-green-300"></span>
              <span className="h-1 rounded-full block w-8 mb-1 bg-gradient-to-tr from-green-600 to-green-300"></span>
              <span className="h-1 rounded-full block w-8 mb-1 bg-gradient-to-tr from-green-600 to-green-300"></span>
            </div>
            <div className="flex items-center">

              {
               user && user.isAdmin ?
               

        
               <ul className="sm:flex space-x-4 hidden items-center">
                <li><Link to = "/" ><a classNames="text-base text-gray-900 font-normal rounded-lg hover:bg-gray-100 flex items-center p-2 group">Home</a></Link></li>
                <li><Link to = "/addProduct" ><a className="text-base text-gray-900 font-normal rounded-lg hover:bg-gray-100 flex items-center p-2 group ">Create Product</a></Link></li>
                <li><Link to = "/allUsers" ><a className="text-base text-gray-900 font-normal rounded-lg hover:bg-gray-100 flex items-center p-2 group ">View All User</a></Link></li>
                <li><Link to = "/AdminItems" ><a className="text-base text-gray-900 font-normal rounded-lg hover:bg-gray-100 flex items-center p-2 group ">View All Products</a></Link></li>
               
              
                </ul>
                :
                <Fragment>
                
                
                {
                  user?
                  <ul className="sm:flex space-x-4 hidden items-center">
                  <li><Link to = "/" ><a classNames="text-base text-gray-900 font-normal rounded-lg hover:bg-gray-100 flex items-center p-2 group ">Home</a></Link></li>
                  <li><Link to = "/items" ><a className="text-base text-gray-900 font-normal rounded-lg hover:bg-gray-100 flex items-center p-2 group ">Items</a></Link></li>
                  <li><Link to = "/myOrder" ><a className="text-base text-gray-900 font-normal rounded-lg hover:bg-gray-100 flex items-center p-2 group ">My Order</a></Link></li>
                
               
                  </ul>
                  :
                  <ul className="sm:flex space-x-4 hidden items-center">
                  <li><Link to = "/" ><a classNames="text-base text-gray-900 font-normal rounded-lg hover:bg-gray-100 flex items-center p-2 group ">Home</a></Link></li>
                  <li><Link to = "/items" ><a className="text-base text-gray-900 font-normal rounded-lg hover:bg-gray-100 flex items-center p-2 group ">Items</a></Link></li>
                  </ul>
                  }

               
              
                </Fragment>
             
              
                 
                }

                <div className="md:flex items-center hidden space-x-4 ml-8 lg:ml-12">
                
              {

              user ?
              <Fragment>
              
               <Link to= "/logout">
                <h1  className="text-text-gray-600  py-2 hover:cursor-pointer px-4 rounded text-white bg-gradient-to-tr from-orange-700 to-orange-300 hover:shadow-lg">
                LOGOUT</h1>
                </Link>
                
                </Fragment>
                :

                <Fragment>

                <Link to= "/login">
                 <h1 className="text-text-gray-600  py-2 hover:cursor-pointer hover:text-orange-300">
                 LOGIN</h1>
                 </Link>
                 
                <Link to= "/register">
                <h1 className="text-text-gray-600  py-2 hover:cursor-pointer px-4 rounded text-white bg-gradient-to-tr from-orange-700 to-orange-300 hover:shadow-lg">
                SIGNUP</h1>
                </Link>
                </Fragment>
                
                }
                
                
                
              </div>
            </div>
          </div>
        </div>
        </nav>
      </header>

      </>
    )
}




