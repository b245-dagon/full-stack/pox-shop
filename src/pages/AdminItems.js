
import { Fragment , useEffect , useState} from "react";

import CollectionItems from "../Components/CollectionItems";





export default function AdminItems(){
    const[product ,setProduct] = useState([])

    


    useEffect(()=>{
        //fetch all products
        fetch(`${process.env.REACT_APP_API_URL}/product/allProducts`,{
            headers:{
                Authorization: `Bearer ${localStorage.getItem('token')}`
            }
        })
        .then(result => result.json())
        .then(data => {
            setProduct(data.map (product => {

                    return (
                        <CollectionItems key = {product._id}adminProductProp = {product}/>
                    )
                }))
        })

    },[])



    return(
        <>
    <Fragment>
    <div className="flex overflow-auto bg-white pt-5">
        <div id="main-content" className="h-full w-full bg-gray-50 relative">
       
            <div className="p-4 bg-white block sm:flex items-center justify-between border-b border-gray-200 lg:mt-1.5">
                <div className="mb-1 w-full">
                    <div className="mb-4">
                         <h1 className="text-xl sm:text-2xl font-semibold text-gray-900">All products</h1>
                     </div>
                <div className="block sm:flex items-center md:divide-x md:divide-gray-100">
                  <form className="sm:pr-3 mb-4 sm:mb-0" action="#" method="GET">
                     <label for="products-search" class="sr-only">Search</label>
                     <div className="mt-1 relative sm:w-64 xl:w-96">
                     <input type="text" name="email" id="products-search" className="bg-gray-50 border border-gray-300 text-gray-900 sm:text-sm rounded-lg focus:ring-cyan-600 focus:border-cyan-600 block w-full p-2.5" placeholder="Search for products"></input>
                    
                    </div>
                  </form>
                </div>
             <div className="flex items-center sm:justify-end w-full mr-4">
            
            <button type="button" className="text-white bg-orange-600 hover:bg-orange-700 focus:ring-4 focus:ring-orange-200 font-medium inline-flex items-center rounded-lg text-sm mb-5 mr-5 px-3 py-2 text-center sm:ml-auto">
            <svg class="-ml-1 mr-2 h-6 w-6" fill="currentColor" viewBox="0 0 20 20" xmlns="http://www.w3.org/2000/svg"><path fill-rule="evenodd" d="M10 5a1 1 0 011 1v3h3a1 1 0 110 2h-3v3a1 1 0 11-2 0v-3H6a1 1 0 110-2h3V6a1 1 0 011-1z" clip-rule="evenodd"></path></svg>
           Add product
            </button>
              </div>
              <div class="flex items-center justify-center">
                <div class="container">
                   
                            <table class="w-full flex-no-wrap sm:bg-white rounded-lg overflow-auto sm:shadow-lg my-5 table-fixed">
                                <thead class="bg-gray-100">
                                    <tr class="bg-orange-200 flex flex-col flex-no wrap sm:table-row rounded-l-lg sm:rounded-none mb-2 sm:mb-0">
                                    <th scope="col" class="p-4 text-left text-xs font-medium text-gray-500 uppercase">Product Name
                                    </th>
                                    <th scope="col" class="p-4 text-left text-xs font-medium text-gray-500 uppercase">Description
                                    </th>
                                    
                                    <th scope="col" class="p-4 text-left text-xs font-medium text-gray-500 uppercase">Price
                                    </th>
                                    <th scope="col" class="p-4 text-left text-xs font-medium text-gray-500 uppercase">Category
                                    </th>
                                    <th scope="col" class="p-4 text-left text-xs font-medium text-gray-500 uppercase">Actions
                                    </th>
                                    
                                    
                                        
                                    </tr>
                                </thead>  
                              
                                    {product}
                               
                            </table>
            
                        </div>
                    </div>
                </div>
            </div>
              
                  
           
        </div>
        
    </div>          
    </Fragment>

        
        </>
    )
}