import Swal from "sweetalert2";
import { useState , useEffect} from "react";
import { useNavigate } from "react-router-dom";

export default function CreateProduct(){

    const [productName , setProductName] = useState('');
    const [description , setDescription] = useState('');
    const [price , setPrice] = useState('');

    const navigate = useNavigate();
    
    const [isActive , setIsActive] = useState(false)

     useEffect(()=>{
    if(productName !== "" && description !== "" && price !==""){
        setIsActive(true)
    }else{
        setIsActive(false)
    }
    },[productName,description, price])

    function addProduct(event){
        event.preventDefault();
    
        fetch(`${process.env.REACT_APP_API_URL}/product/register`, {
            method: "POST",
            body: JSON.stringify({
             
              productName: productName,
              description:  description,
              price: price
            }),
            headers:{
                'Content-Type' : 'application/json',
                Authorization: `Bearer ${localStorage.getItem('token')}`
            }
          })
          .then(result => result.json())
          .then(data => {
          
            if(data.isAdmin){
              Swal.fire({
                title: "Failed to Add Item",
                icon: "error",
                text: "Please try Again"
              })
              navigate("/addProduct")
            } else {
              Swal.fire({
                title: "Successfully Added",
                icon: "success",
                text: "The Product has Successfully Added"
              })
            }
          })
        }
    



    return(
        <>
        <div className="items-center justify-center py-10 px-4 sm:px-6 lg:px-8">
        <div id='section2' class="p-8 mt-6 lg:mt-0 rounded shadow bg-white">

        <form  onSubmit={event => addProduct(event)}>

         <div className="md:flex mb-6">
        <div className="md:w-1/3">
            <label className="block text-gray-600 font-bold md:text-left mb-3 md:mb-0 pr-4">
                Product Name
            </label>
        </div>
        <div className="md:w-2/3">
            <input 
             value = {productName}
            onChange ={event => setProductName(event.target.value)} 
            class="form-input block w-full border border-gray-300 px-3 py-2 text-gray-900 placeholder-gray-500 focus:z-10 focus:border-indigo-500 focus:outline-none focus:ring-indigo-500" type="text"></input>
            <p className="py-2 text-sm text-gray-600">add product name here</p>
        </div>
    </div>

    <div className="md:flex mb-6">
        <div className="md:w-1/3">
            <label className="block text-gray-600 font-bold md:text-left mb-3 md:mb-0 pr-4">
                Price
            </label>
        </div>
        <div className="md:w-2/3">
            <input
             value = {price}
             onChange ={event => setPrice(event.target.value)}  
             className="form-input block w-full border border-gray-300 px-3 py-2 text-gray-900 placeholder-gray-500 focus:z-10 focus:border-indigo-500 focus:outline-none focus:ring-indigo-500" type="number"></input>
            <p className="py-2 text-sm text-gray-600">add price here</p>
            
        </div>
    </div>

    <div className="md:flex mb-6">
        <div className="md:w-1/3">
            <label className="block text-gray-600 font-bold md:text-left mb-3 md:mb-0 pr-4" >
                Description
            </label>
        </div>
        <div className="md:w-2/3">
            <textarea 
            value = {description}
            onChange ={event => setDescription(event.target.value)}   
            className="form-textarea block w-full focus:bg-white border border-gray-300"rows="8"></textarea>
            <p className="py-2 text-sm text-gray-600">add product details</p>
        </div>
    </div>

    <div className="md:flex md:items-center">
        <div className="md:w-1/3"></div>
        <div className="md:w-2/3">
            <button className="shadow bg-yellow-700 hover:bg-yellow-500 focus:shadow-outline focus:outline-none text-white font-bold py-2 px-4 rounded" type="submit">
                Save
            </button>
        </div>
    </div>
     </form>

    </div>
    </div>

    
  
        </>
    )
}