




export default function InActiveProduct (){

    const[product ,setProduct] = useState([])


    useEffect(()=>{
        //fetch all products
        fetch(`${process.env.REACT_APP_API_URL}/product`)
        .then(result => result.json())
        .then(data => {
          
            
            setProduct(data.map (product => {

                    return (
                        <Items key = {product._id}productProp = {product}/>
                    )
                }))
        })

    },[])

    return(

        <Fragment>
        <div className="bg-orange-100">
        
        <h1 className="text-center">In Active Products</h1>
        
        <div className=" mx-14 grid md:grid-cols-2 lg:grid-cols-3  gap-4 content-evenly min-w-sm max-w-fit">
           
                 {product}
           
        </div>
        </div>
        
       
        </Fragment>

        

        

    )

}