
import { Link } from 'react-router-dom'
import {  useState ,useEffect, useContext} from 'react';
import { useNavigate} from 'react-router-dom';
import UserContext from '../UserContext';
import Swal from 'sweetalert2'


export default function Login() {

  const [email , setEmail] = useState('');
  const [password , setPassword] = useState('');

  const navigate = useNavigate();


  // Allows us to consume the UserContext object and its properties for user validation

  const {user, setUser} = useContext(UserContext)


  // Button useState
  const [isActive , setIsActive] = useState(false)

  useEffect(()=>{
      if(email !== "" && password !== ""){
          setIsActive(true)
      }else{
          setIsActive(false)
      }

  },[email,password])

  function login(event){
      event.preventDefault();


      //Process a fetch request to corresponding back end API
      fetch(`${process.env.REACT_APP_API_URL}/user/login`,{
          method: 'POST',
          headers:{
              'Content-Type' : 'application/json'
          },
          body: JSON.stringify({
              email: email,
              password: password
          })

      }).then(result => result.json())
      .then(data => {
         

          if(data === false){

              Swal.fire({
                  title: "Wrong Email Or Password ",
                  icon: "error",
                  text: "Please try again"
              })
           
             
          }else{
              localStorage.setItem('token',data.auth)
              retrieveUserDetails(localStorage.getItem('token'))
             
              
              Swal.fire({
                  title: "Authentication successful",
                  icon: 'success',
                  text: "Welcome to Pox Shop"
                 
              })
              navigate("/");
              
             
              
          }
      })


  
      
     
  }

  const retrieveUserDetails = (token) =>{
       // the token sent as part of the request; header information
       fetch(`${process.env.REACT_APP_API_URL}/user/details`,{
          headers:{
              Authorization: `Bearer ${token}`
          }
       })
       .then(result => 
          result.json()
       ).then(data => {
          

          setUser({
              id:data._id,
              isAdmin: data.isAdmin
          })
       })
       
  }

return (
   <> 
    
      <div className="flex min-h-full items-center justify-center py-12 px-4 sm:px-6 lg:px-8">
        <div className="w-full max-w-md space-y-8">
          <div>
           
            <h2 className="mt-6 text-center text-3xl font-bold tracking-tight text-gray-900">
             Log in to our Store
            </h2>
            <p className="mt-2 text-center text-sm text-gray-600">
              Or{' '}
              <Link to = "/register"><a href="#" className="font-medium text-orange-500 hover:text-orange-900">
                If you don't have account Please register 
              </a></Link>
            </p>
          </div>
          <form className="mt-8 space-y-6" onSubmit={event => login(event)}>
            <input type="hidden" name="remember" defaultValue="true" />
            <div className="-space-y-px rounded-md shadow-sm text-center">
              <div>
                <label htmlFor="email-address" className="not-sr-only">
                  Email address
                </label>
                <input
                  id="email-address"
                  name="email"
                  type="email"
                  autoComplete="email"
                  value = {email}
                  onChange ={event => setEmail(event.target.value)} 
                  required
                  className="relative block w-full appearance-none rounded-none rounded-t-md border border-gray-300 px-3 py-2 text-gray-900 placeholder-gray-500 focus:z-10 focus:border-indigo-500 focus:outline-none focus:ring-indigo-500 sm:text-sm"
                  placeholder="Email address"
                />
              </div>
              <div >
                <label htmlFor="password" className="not-sr-only">
                Password
                </label>
                <input
                  id="password"
                  name="password"
                  type="password"
                  autoComplete="current-password"
                  value = {password}
                  onChange ={event => setPassword(event.target.value)} 
                  required
                  className="relative block w-full appearance-none rounded-none rounded-b-md border border-gray-300 px-3 py-2 text-gray-900 placeholder-gray-500 focus:z-10 focus:border-indigo-500 focus:outline-none focus:ring-indigo-500 sm:text-sm"
                  placeholder="Password"
                />
              </div>
            </div>

            <div className="flex items-center justify-between">
              <div className="flex items-center">
                <input
                  id="remember-me"
                  name="remember-me"
                  type="checkbox"
                  className="h-4 w-4 rounded border-gray-300 text-indigo-600 focus:ring-indigo-500"
                />
                <label htmlFor="remember-me" className="ml-2 block text-sm text-gray-900">
                  Remember me
                </label>
              </div>

              <div className="text-sm">
                <a href="#" className="font-medium text-orange-500 hover:text-orange-900">
                  Forgot your password?
                </a>
              </div>
            </div>

            <div>
              <button
                type="submit"
                className="group relative flex w-full justify-center rounded-md border border-transparent bg-orange-400 py-2 px-4 text-sm font-medium text-white hover:bg-orange-300 focus:outline-none focus:ring-2 focus:ring-indigo-500 focus:ring-offset-2"
              >
              
                Sign in
              </button>
            </div>
          </form>
        </div>
      </div>
    
    </>
  )
}
 


