import { Fragment , useEffect , useState} from "react";
import ViewOrders from "../Components/ViewOrders";



export default function MyOrder(){

    const [order, setOrder] =useState([])

    useEffect(()=>{
        //fetch all products
        fetch(`${process.env.REACT_APP_API_URL}/user/myOrder`,{
            headers:{
                Authorization: `Bearer ${localStorage.getItem('token')}`
            }
        })
        .then(result => result.json())
        .then(data => {
            setOrder(data.map (order => {

                    return (
                        <ViewOrders key = {order._id}OrderProp = {order}/>
                    )
                }))
        })

    },[])

    return(
        <>
        <Fragment>
    <div className="flex overflow-hidden bg-white pt-5">
        <div id="main-content" className="h-full w-full bg-gray-50 relative">
       
            <div className="p-4 bg-white block sm:flex items-center justify-between border-b border-gray-200 lg:mt-1.5">
                <div className="mb-1 w-full">
                    <div className="mb-4">
                         <h1 className="text-xl sm:text-2xl font-semibold text-gray-900">All Users</h1>
                     </div>
                <div className="block sm:flex items-center md:divide-x md:divide-gray-100">
                  <form className="sm:pr-3 mb-4 sm:mb-0" action="#" method="GET">
                     <label for="products-search" class="sr-only">Search</label>
                     <div className="mt-1 relative sm:w-64 xl:w-96">
                     <input type="text" name="email" id="user-search" className="bg-gray-50 border border-gray-300 text-gray-900 sm:text-sm rounded-lg focus:ring-cyan-600 focus:border-cyan-600 block w-full p-2.5" placeholder="Search for User"></input>

                    </div>
                  </form>
                  <div className="flex items-center sm:justify-end w-full mr-4">
            
            <button type="button" className="text-white bg-orange-600 hover:bg-orange-700 focus:ring-4 focus:ring-orange-200 font-medium inline-flex items-center rounded-lg text-sm mr-5 px-3 py-2 text-center sm:ml-auto">
            <svg class="-ml-1 mr-2 h-6 w-6" fill="currentColor" viewBox="0 0 20 20" xmlns="http://www.w3.org/2000/svg"><path fill-rule="evenodd" d="M10 5a1 1 0 011 1v3h3a1 1 0 110 2h-3v3a1 1 0 11-2 0v-3H6a1 1 0 110-2h3V6a1 1 0 011-1z" clip-rule="evenodd"></path></svg>
           My Orders
            </button>
                </div>
             
              </div>
              <div class="flex items-center justify-center mt-4">
                <div class="container">
                   
                            <table class="min-w-full border-collapse block md:table flex-no-wrap table-fixed">
                                <thead class="bg-orange-100 block md:table-header-group ">
                                    <tr class="border border-grey-500 md:border-none block md:table-row absolute -top-full md:top-auto -left-full md:left-auto  md:relative ">
                                    <th scope="col" class="p-4 text-left text-xs font-medium text-gray-500 uppercase">Order ID
                                    </th>
                                    <th scope="col" class="p-4 text-left text-xs font-medium text-gray-500 uppercase">User ID
                                    </th>
                                    <th scope="col" class="p-4 text-left text-xs font-medium text-gray-500 uppercase">Product ID
                                    </th>
                                    <th scope="col" class="p-4 text-left text-xs font-medium text-gray-500 uppercase">Total Amount
                                    </th>
                                  
                                    
                                    
                                        
                                    </tr>
                                </thead>  
                              
                                    {order}
                               
                            </table>
            
                        </div>
                    </div>
                </div>
            </div>
              
                  
           
        </div>
        
    </div>      
    </Fragment>

        
        
        </>
        

    )
}