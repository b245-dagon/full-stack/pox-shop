

import { Fragment , useEffect , useState} from "react";
import Items from "../Components/Items";


export default function Product (){

    const[product ,setProduct] = useState([])
    const [cart ,setCart] = useState([])

    const handleAddtoCart = product =>{
        setCart(prev => {
          const findProductInCart = prev.find(item=>item.id === product.id)
    
          if(findProductInCart){
            return prev.map(item=>item.id === product.id? {...item,amount:item.amount+1}:item)
          }
          return [...prev,{ ...product, amount:1}]
        })
      }


    useEffect(()=>{
        //fetch all products
        fetch(`${process.env.REACT_APP_API_URL}/product`)
        .then(result => result.json())
        .then(data => {
            
            
            setProduct(data.map (product => {

                    return (
                        <Items 

                        handleAddtoCart={handleAddtoCart}
                        
                        key = {product._id}productProp = {product}/>
                    )
                }))
        })

    },[])


    return(

        <Fragment>

            <div class="pt-20  bg-white mb-5">
            <h1 class="text-center text-2xl font-bold text-gray-800">All Products</h1>
            </div>
            <div className="flex flex-wrap items-center overflow-x-auto overflow-y-hidden justify-center  bg-orange-100 text-gray-800">
            <a className="flex items-center bg-orange-200 rounded-md flex-shrink-0 mx-2 px-5 py-3 space-x-2 text-gray-600 hover:bg-orange-400">
            <span>For Cats</span>   
           </a>
           <a className="flex items-center bg-orange-200 rounded-md flex-shrink-0 mx-2 px-5 py-3 space-x-2 text-gray-600 hover:bg-orange-400">
            <span>For Dogs</span>   
           </a>
           <a className="flex items-center bg-orange-200 rounded-md flex-shrink-0 mx-2  px-5 py-3 space-x-2 text-gray-600 hover:bg-orange-400 ">
            <span>Accessories</span>   
           </a>

        </div>
           
     
        
        
        <div className=" mx-14 grid md:grid-cols-2 lg:grid-cols-4  gap-4 content-evenly min-w-sm max-w-fit">
           
                 {product}
           
        </div>

       
      

       
        
       
        </Fragment>

        

        

    )
}